package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.
    /** this tests whether drawing just a horitonal line at coordinate (0,0)
     * completes a box at coordinate (0,0) or not.
     * The game shouldn't complete a box and return false. */
    @DisplayName("Just horizontal line at [0][0], complete a box at [0][0]")
    @Test
    public void testBoxComplete() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);
        grid.drawHorizontal(0, 0, 1);
        assertEquals(false, grid.boxComplete(0, 0));
    }

    /** This test method tests if the drawHorizontal method in DotsAndBoxesGrid.java class returns an
     * IllegalArgumentException error when the user tries to draw a horizontal line on top of another
     * (on top of an existing line). */
    @DisplayName("Fails to throw an exception when attempting to draw a horizontal line on top of another")
    @Test
    public void testRedrawingTheLineHoriz() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);
        grid.drawHorizontal(1, 1, 1);
        assertThrows(IllegalArgumentException.class, () -> {
            grid.drawHorizontal(1, 1, 1);
        });
    }

    /** This tests if the drawVertical method in DotsAndBoxesGrid.java class returns an
     * IllegalArgumentException error when the user tries to draw a vertical line on top
     * of another. */
    @DisplayName("Fails to throw an exception when attempting to draw a Vertical line on top of another")
    @Test
    public void testRedrawingTheLineVerti() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);
        grid.drawVertical(0, 1, 1);
        assertThrows(IllegalArgumentException.class, () -> {
            grid.drawVertical(0, 1, 1);
        });
    }
}
